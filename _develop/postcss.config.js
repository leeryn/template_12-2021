const progress = require('postcss-progress');
const csso = require('postcss-csso');
const paths = require('./paths');
const baseFontSize = 10; // -  rem計算用

const pcDesignWidth = 1440; // -  vw風にpc計算する用
const pcDesignHeight = 1440; // -  vw風にpc計算する用

const tbDesignWidth = 1366; // -  vw風にtablet計算する用
const tbDesignHeight = 1366; // -  vw風にtablet計算する用

const spDesignWidth = 768; // -  vw計算用
const spDesignHeight = 736; // -  vh計算用

module.exports = {
  map: false,
  plugins: [
    progress.start(),
    require('postcss-import')(),
    require('stylelint')(),
    require('postcss-nested')(),
    require('postcss-simple-vars')(),
    require('postcss-custom-media')(),
    require('postcss-hexrgba')(),
    require('postcss-percentage')(),
    require('postcss-gradient-transparency-fix')(),
    require('postcss-mixins')(),
    require('postcss-functions')({
      functions: {
        px(num) {
          return `${((num * pcDesignWidth) / tbDesignWidth).toFixed(0)}px`;
        },
        rem(num) {
          return `${num / baseFontSize}rem`;
          // return `${(num * pcDesignWidth) / tbDesignWidth / baseFontSize}rem`;
        },
        pw(num, fix = false) {
          if (fix) {
            return `calc( var(--vw) * ${num} )`;
          } else {
            return `calc( var(--vw) * ${(num / pcDesignWidth) * 100} )`;
          }
        },
        ph(num, fix = false) {
          if (fix) {
            return `calc( var(--vh) * ${num} )`;
          } else {
            return `calc( var(--vh) * ${(num / pcDesignHeight) * 100} )`;
          }
        },
        vwTb(num, fix = false) {
          if (fix) {
            return `calc( var(--vw) * ${num} )`;
          } else {
            return `calc( var(--vw) * ${(num / tbDesignWidth) * 100} )`;
          }
        },
        vhTb(num, fix = false) {
          if (fix) {
            return `calc( var(--vh) * ${num} )`;
          } else {
            return `calc( var(--vh) * ${(num / tbDesignHeight) * 100} )`;
          }
        },
        vw(num, fix = false) {
          if (fix) {
            return `calc( var(--vw) * ${num} )`;
          } else {
            return `${(num / spDesignWidth) * 100}vw`;
          }
        },
        vh(num, fix = false) {
          if (fix) {
            return `calc( var(--vh) * ${num} )`;
          } else {
            return `calc( var(--vh) * ${(num / spDesignHeight) * 100} )`;
          }
        },
        lh(fz, psd) {
          return (psd / fz).toFixed(2);
        },
        ls(psd) {
          return `${psd / 1000}em`;
        },
      },
    }),
    require('postcss-momentum-scrolling')(['auto', 'scroll']),
    require('postcss-will-change-transition')(),
    require('autoprefixer')(),
    require('postcss-cachebuster')({
      type: 'checksum',
      imagesPath: `${paths.appDest}`,
    }),
    require('postcss-reporter')({
      clearReportedMessages: true,
    }),
    csso({
      restructure: false,
    }),
    progress.stop(),
  ],
};
