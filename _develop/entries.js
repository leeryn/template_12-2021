/* eslint-disable @typescript-eslint/no-unsafe-argument */
const path = require('path');
const glob = require('glob');

function collectEntries() {
  const entries = {
    bundle: ['./src/js/main.js']
  };

  const filePaths = glob.sync('./src/js/pages/**/!(_)**{.ts,.js}');

  filePaths.reduce(function (acc, currentPath) {
    const dirArr = path.parse(currentPath).dir.split('/');
    if (path.parse(currentPath).dir.includes('/index')) return acc;
    if (path.parse(currentPath).name === 'index') {
      acc[dirArr.slice(-1)[0]] = [currentPath];
    } else {
      const name = `${dirArr.slice(-1)[0]}_${path.parse(currentPath).name}`;
      acc[name] = [currentPath];
    }
    return acc;
  }, entries);

  return entries;
}

module.exports = collectEntries;
