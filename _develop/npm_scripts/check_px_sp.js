/* eslint-disable  */

const fs = require('fs');
const colors = require('./colors');

const readdirRecursively = (dir, files = []) => {
  const paths = fs.readdirSync(dir);
  const dirs = [];
  for (const path of paths) {
    const stats = fs.statSync(`${dir}/${path}`);
    if (stats.isDirectory()) {
      dirs.push(`${dir}/${path}`);
    } else {
      files.push(`${dir}/${path}`);
    }
  }
  for (const d of dirs) {
    files = readdirRecursively(d, files);
  }
  return files;
};

const files = readdirRecursively('./src/css');
const checkError = () => {
  for (const file of files) {
    if (file.includes('_sp.css')) {
      let css = fs.readFileSync(file, { encoding: 'utf-8' });
      css = css
        .replace(/ 1px/g, '')
        .replace(/ 2px/g, '');
      if (file.includes('_layout/_sp.css')) {
        css = css.replace(/min-width: 320px/g, '');
      }
      if (css.includes('px')) {
        return true;
      }
    }
  }
};
if (checkError()) {
  console.log(`${colors.redBg}${colors.white}Please check the px to vw() conversion of the files below${colors.reset}\n`);
  for (const file of files) {
    if (file.includes('_sp.css')) {
      let css = fs.readFileSync(file, { encoding: 'utf-8' });
      css = css
        .replace(/ 1px/g, '')
        .replace(/ 2px/g, '');
      if (file.includes('_layout/_sp.css')) {
        css = css.replace(/min-width: 320px/g, '');
      }
      if (css.includes('px')) {
        console.log(`${colors.red}${file}${colors.reset}`);
      }
    }
  }
  console.log(`\n\n`);
}
