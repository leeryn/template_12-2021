/* ===================================================================
こういう構造で作っておくと、newした時自動でアコーディオンが実装される。

【pug】
.accordion
  .accordion__trigger(開閉ボタン。.activeが付いたり消えたり)
  .accordion__target
    .accordion__inner

【css】
.accordion {
  box-sizing: border-box;
  position: relative;
  &__trigger {
    cursor: pointer;
    &.active {
      active時
    }
  }
  &__target {
    overflow: hidden;
    height: 0;
    box-sizing: border-box;
  }
}
=================================================================== */
import gsap from 'gsap';
import Util from '../utils/util';

/**
 * アコーディオン
 * 上の構造で作ればimportしてnewするだけで使える。 new Accordion();
 * @export
 * @class Accordion
 */
export default class Accordion {
  private accordionInstance: AccordionElem[] | null = [];

  constructor() {
    const accordionList: HTMLElement[] = Array.prototype.slice.call(document.getElementsByClassName('accordion'));
    for (const accordion of accordionList) {
      if (this.accordionInstance) {
        this.accordionInstance.push(new AccordionElem(accordion));
      }
    }
  }

  public destroy = (): void => {
    if (!this.accordionInstance) return;
    for (const elem of this.accordionInstance) {
      elem.destroy();
    }
    this.accordionInstance = null;
  };
}

/* =================================================================== */

class AccordionElem {
  private trigger: HTMLElement | null;
  private wrapper: HTMLElement | null;
  private inner: HTMLElement | null;
  private height?: number;

  constructor(node: HTMLElement) {
    this.trigger = Array.prototype.slice.call(node.getElementsByClassName('accordion__trigger'))[0];
    this.wrapper = Array.prototype.slice.call(node.getElementsByClassName('accordion__target'))[0];
    this.inner = Array.prototype.slice.call(node.getElementsByClassName('accordion__inner'))[0];
    this.height = this.inner?.offsetHeight;

    if (!this.trigger || !this.wrapper || !this.inner) {
      Util.warn('nullCheckError: accordion.ts : line58');
      return;
    }

    this.trigger.addEventListener('click', this.toggle, false);
    window.addEventListener('resize', this.resize, false);
  }

  private toggle = (): void => {
    if (!this.trigger || !this.wrapper || !this.inner) {
      return;
    }

    gsap.killTweensOf(this.wrapper);

    if (!this.trigger.classList.contains('active')) {
      this.trigger.classList.add('active');
      gsap.to(this.wrapper, 0.4, { height: this.height, ease: 'power1.out' });
    } else {
      this.trigger.classList.remove('active');
      gsap.to(this.wrapper, 0.4, { height: 0, ease: 'power1.in' });
    }
  };

  public destroy = (): void => {
    if (!this.trigger || !this.wrapper || !this.inner) return;

    this.trigger.removeEventListener('click', this.toggle);
    gsap.killTweensOf(this.wrapper);
    this.wrapper.removeAttribute('style');

    this.trigger = null;
    this.wrapper = null;
    this.inner = null;
  };

  // resize height according to screen size
  private resize = (): void => {
    if (!this.wrapper) return;
    this.height = this.inner?.offsetHeight;
    if (this.wrapper.clientHeight !== 0) {
      this.wrapper.style.height = `${this.height}px`;
    }
  };
}
