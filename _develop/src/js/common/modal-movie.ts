import gsap from 'gsap';
import Util from '../utils/util';
import { Global } from '../utils/helper/events-helper';
export default class ModalMovie {
  constructor() {
    this.init();
    Global.addEventListener('load-more', () => {
      this.init();
    });
  }
  private init = () => {
    const triggers: HTMLElement[] = Array.prototype.slice.call(document.getElementsByClassName('modal-trigger'));
    for (const trigger of triggers) {
      trigger.addEventListener('click', this.openModal, false);
    }
  };
  private openModal = (e: any): void => {
    e.preventDefault();
    const movieCurrent: string | null = e.currentTarget?.dataset?.movie;
    if (movieCurrent) {
      new Modal(movieCurrent);
    } else {
      Util.warn('modal-player.ts : data-movieが無いです');
    }
  };
}

class Modal {
  private player: HTMLElement | null = null;
  private bg: HTMLElement | null = null;
  private close: HTMLElement | null = null;

  constructor(id: string) {
    const html = `
      <div class="modal-player" id="modal-player">
        <p class="modal-player__bg" id="modal-player__bg"></p>
        <div class="modal-player__window">
          <iframe src="https://www.youtube.com/embed/${id}?autoplay=1&playsinline=1&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          <p class="modal-player__close" id="modal-player__close"></p>
        </div>
      </div>
    `;
    document.body.insertAdjacentHTML('beforeend', html);

    this.player = document.getElementById('modal-player');
    this.bg = document.getElementById('modal-player__bg');
    this.close = document.getElementById('modal-player__close');

    gsap.from(this.player, 0.35, { opacity: 0, ease: 'power1.out' });

    this.bg?.addEventListener('click', this.prepClose, false);
    this.close?.addEventListener('click', this.prepClose, false);
  }

  private prepClose = (): void => {
    gsap.to(this.player, 0.35, { opacity: 0, ease: 'power1.in', onComplete: this.purge });
  };

  private purge = (): void => {
    this.bg?.removeEventListener('click', this.prepClose);
    this.close?.removeEventListener('click', this.prepClose);

    if (this.player) document.body.removeChild(this.player);

    this.player = null;
    this.bg = null;
    this.close = null;
  };
}
