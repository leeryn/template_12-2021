export default class Index {
  /**
   * Creates an instance of Index.
   */
  constructor() {
    console.log('Second page');
  }
}

window.addEventListener('DOMContentLoaded', () => {
  new Index();
});
