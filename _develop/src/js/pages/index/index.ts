import { selector } from '../../utils/helper/function-helper';
import Swiper, { Pagination, EffectFade, Autoplay } from 'swiper';
import IntersectElements from '../../utils/logic/intersect-elem';
export default class Index {
  /**
   * Creates an instance of Index.
   */
  private slider: HTMLElement[] | null;
    private section: HTMLElement[] | null;
  constructor() {
    this.slider = Array.prototype.slice.call(document.querySelectorAll('.b-visual-slider .swiper-slide'));
        this.section = Array.prototype.slice.call(document.querySelectorAll('.js-photo'));
    this.init();
  }
  public init = (): void => {
    new IntersectElements();
    this.mvSlide();
    window.addEventListener('scroll',()=> {
      this.handleScrollSection();
    });
  };

  private isPartiallyVisible = (el: HTMLElement) => {
    const elementBoundary = el.getBoundingClientRect();
    const top: number = elementBoundary.top;
    const bottom: number = elementBoundary.bottom;
    const height: number = elementBoundary.height;
    // return true when element in display
    // return top + height >= 0 && height + window.innerHeight >= bottom;
    // return true when element bottom on bottom display
    return top + height >= 0 && window.innerHeight >= bottom;
    // return true when element bottom on bottom display
    // return top - (headerHeight/2) < 0 && bottom - (headerHeight/2) > 0;
  };

  private mvSlide = () => {
    if (!this.slider) return;
    if (this.slider.length === 1) {
      new Swiper('.b-visual-slider', {
        loop: false,
        allowTouchMove: false
      });
    } else {
      new Swiper('.b-visual-slider', {
        // slidesPerView: 1,
        autoplay: {
          delay: 5000,
          disableOnInteraction: false
        },
        speed: 600,
        loop: true,
        pagination: {
          el: '.b-visual-pagi',
          clickable: true
        },
        mousewheel: {
          invert: false
        },
        effect: 'fade',
        modules: [Pagination, EffectFade, Autoplay]
      });
    }
  };

  private handleScrollSection = () => {
    const mvTop: HTMLElement = selector('.js-mv');
    const mvTopPos: DOMRect = mvTop.getBoundingClientRect();

    this.section?.forEach((el) => {
      if (this.isPartiallyVisible(el)) {
        el.classList.add('fixed');
        if(mvTop && mvTopPos.bottom - window.innerHeight > 0) {
          el.classList.remove('fixed');
        }
      } else {
        el.classList.remove('fixed');
      }
    });
  };

}
