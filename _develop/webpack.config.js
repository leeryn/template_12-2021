// const path = require('path');
const webpack = require('webpack');
const paths = require('./paths');
const TerserPlugin = require('terser-webpack-plugin');
const DuplicatePackageCheckerPlugin = require('duplicate-package-checker-webpack-plugin');
const EventHooksPlugin = require('event-hooks-webpack-plugin');
const shouldUseSourceMap = false;
const ESLintPlugin = require('eslint-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
// const shouldUseSourceMap = false;
const Dotenv = require('dotenv-webpack');

const colors = require('./npm_scripts/colors');
const collectEntries = require('./entries');

const noticeMessage = `
${colors.blue}****************   ${colors.white}WATCH MODE${colors.blue}   **********************${colors.red}
    process.env.NODE_ENV : development
    Don't forget ${colors.green}npm run build

    ${colors.blueBg}${colors.black}【watchでのJSの書換を確認】${colors.reset}

    ${colors.red}アップロード/push前に必ず ${colors.green}npm run build${colors.red}
    を実行すること。
    ${colors.blue}******************************************************${colors.reset}
`;

const entries = collectEntries();
console.log(entries);
module.exports = (env) => {
  process.env.NODE_ENV = env.production ? 'production' : 'development';

  const plugins = [
    new Dotenv({
      path: process.env.NODE_ENV === 'development' ? './.env.test' : './.env.prod'
    }),
    // read subDirectory in js file
    new webpack.DefinePlugin({
      'process.env.subDirectory': JSON.stringify(paths.subDirectory)
    }),
    new DuplicatePackageCheckerPlugin(),
    new EventHooksPlugin({
      done: () => {
        if (process.env.NODE_ENV === 'production') return;
        console.log(noticeMessage);
      }
    })
  ];

  if (process.env.NODE_ENV === 'development') {
    plugins.push(new ForkTsCheckerWebpackPlugin({ async: false }));
    plugins.push(new ESLintPlugin({ extensions: ['.ts', '.js', '.tsx', '.jsx'], exclude: 'node_modules/' }));
  }

  return {
    target: ['web', 'es6'],
    mode: process.env.NODE_ENV,
    bail: false,
    devtool: process.env.NODE_ENV === 'development' ? 'inline-source-map' : false,
    entry: entries,
    output: {
      path: paths.appBuild,
      publicPath: `/${paths.subDirectory}`,
      filename: `${paths.assetPath}/js/[name].js`,
      chunkFilename: (pathData) => {
        return pathData.chunk.name.includes('vendor') || pathData.chunk.name.includes('bundle')
          ? `${paths.assetPath}/js/[name].js`
          : process.env.NODE_ENV === 'development'
          ? `${paths.assetPath}/js/[name].js`
          : `${paths.assetPath}/js/[name].[chunkhash:8].js`;
      }
    },
    cache: {
      type: 'filesystem',
      buildDependencies: {
        config: [__filename]
      },
      version: '1.0'
    },
    optimization: {
      minimizer: [
        new TerserPlugin({
          parallel: true,
          extractComments: false,
          terserOptions: {
            parse: {
              ecma: 6
            },
            compress: {
              ecma: 6,
              warnings: false,
              comparisons: false,
              inline: 2
            },
            output: {
              ecma: 6,
              comments: false,
              ascii_only: true
            }
          }
        })
      ],
      splitChunks: {
        name: 'vendor',
        chunks: 'all',
        cacheGroups: {
          default: false
        }
      },
      runtimeChunk: 'single'
    },
    module: {
      strictExportPresence: true,
      rules: [
        { parser: { requireEnsure: false } },
        {
          test: /\.(js|mjs|jsx|ts|tsx)$/,
          exclude: /(node_modules)/,
          use: [
            {
              loader: 'swc-loader',
              options: {
                cacheDirectory: true
              }
            }
          ]
        }
      ]
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js', '.jsx']
    },
    plugins,
    performance: false
  };
};
